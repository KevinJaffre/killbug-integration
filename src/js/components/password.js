function password() {

  if ($('.js-toggle-password').length) {

    $('.js-toggle-password').click(function() {
      if ($('.js-toggle-password-input').attr('type') == "password") {
        $('.js-toggle-password-input').attr('type', 'text');
      } else {
        $('.js-toggle-password-input').attr('type', 'password');
      }
    });

  }

}

export default password;
